<?php

/**
 * @file
 * Contains the SearchApiAlterNodeStatus class.
 */

/**
 * Exclude unpublished nodes from node indexes.
 */
class workbenchSearchAPIDataAlter extends SearchApiAbstractAlterCallback {

  /**
   * Check whether this data-alter callback is applicable for a certain index.
   *
   * Returns TRUE only for indexes on nodes.
   *
   * @param SearchApiIndex $index
   *   The index to check for.
   *
   * @return boolean
   *   TRUE if the callback can run on the given index; FALSE otherwise.
   */
  public function supportsIndex(SearchApiIndex $index) {
    return $index->item_type === 'search_api_et_node';
  }

  /**
   * Alter items before indexing.
   *
   * Items which are removed from the array won't be indexed, but will be marked
   * as clean for future indexing.
   *
   * @param array $items
   *   An array of items to be altered, keyed by item IDs.
   */
  public function alterItems(array &$items) {
    foreach ($items as $id => &$item) {
      // if using workbench and we aren't the deployment destination
      // when deployment is enabled the destination site status in 
      // workbench_moderation_node_history always seems to be draft and 
      // published == 0. We still want these indexed so we send them to
      // the regular published/status check.
      if (workbench_moderation_node_type_moderated($item->type) && !module_exists('wetkit_deployment_destination')) {
        // if the node being saved isn't being published we need to do some checking
        // otherwise (published) it will be indexed
        if ($item->workbench_moderation['current']->state <> 'published') {
          // check if there is a published revision
          $query = db_select('workbench_moderation_node_history', 'wb');
          $query->fields('wb', array('nid', 'vid'))
            ->condition('wb.nid', $item->nid, '=')
            ->condition('wb.published', '1', '=')
            ->orderBy('wb.vid', 'DESC')
            ->range(0,1)
          ;
          $revision = $query
            ->execute()
            ->fetchAll();
          // if there is not a published revision delete it from the index
          if (!$revision) {
            unset($items[$id]);
          }
          // otherwise load the published revision
          else {
            $entity = entity_revision_load('node', $revision[0]->vid);
            module_load_include('inc', 'search_api_et', 'includes/SearchApiEtHelper.php');
            // This method might receive two different types of item IDs depending on
            // where it is being called from. For example, when called from
            // search_api_index_specific_items(), it will receive multilingual IDs
            // (with language prefix, like "2/en"). On the other hand, when called from
            // a processor (for example from SearchApiHighlight::getFulltextFields()),
            // the IDs won't be multilingual (no language prefix), just standard
            // entity IDs instead. Therefore we need to account for both cases here.
            // Case 1 - language is in item ID.
            if (SearchApiEtHelper::isValidItemId($id)) {
              $entity_id = SearchApiEtHelper::splitItemId($id, SearchApiEtHelper::ITEM_ID_ENTITY_ID);
              $item_languages[$entity_id][] = SearchApiEtHelper::splitItemId($id, SearchApiEtHelper::ITEM_ID_LANGUAGE);
            }
            // Case 2 - no language in item ID.
            else {
              $entity_id = $id;
              $item_languages[$id] = array();
            }
            foreach($item_languages[$entity_id] as $lang) {
              // If the translation isn't published we delete it from the index
              if (!$entity->translations->data[$lang]['status']) {
                unset($items[$id]);
              }
              // There is a published translation
              else {
                // add the search api et id and search api language
                $entity->search_api_et_id = $item->search_api_et_id;
                $entity->search_api_language = $item->search_api_language;
                $items[$id] = $entity;
              }
            }
          }
        }
      }
      // if not using workbench or deployment destination, and it isn't published remove it from the index
      elseif  (empty($item->status)) {
        unset($items[$id]);
      }
    }
  }
}
